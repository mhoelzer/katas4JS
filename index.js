// sample variables
const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit"


// num1: array of gCC
let span1 = document.createElement("span");
let text1 = document.createTextNode(JSON.stringify(gotCitiesCSV.split(",")));
/* 
let arrayGot = gotCitiesCSV.split(",")
let text1 = document.createTextNode(JSON.stringify(arrayGot)); 
OR
let arrayGot = JSON.stringify(gotCitiesCSV.split(","))
let text1 = document.createTextNode(arrayGot);
*/
span1.appendChild(text1);
let placeHere1 = document.getElementById("num1");
placeHere1.appendChild(span1);
// take string, make array, use stringify 

// num2: array of words of bT
let span2 = document.createElement("span");
let text2 = document.createTextNode(JSON.stringify(bestThing.split(" ")));
span2.appendChild(text2);
let placeHere2 = document.getElementById("num2");
placeHere2.appendChild(span2);

// num3: semicolons separate gCC
let span3 = document.createElement("span");
let text3 = document.createTextNode(JSON.stringify(gotCitiesCSV.replace(/,/g,"; ")));
span3.appendChild(text3);
let placeHere3 = document.getElementById("num3");
placeHere3.appendChild(span3);

// num4: CSV of lCA
let span4 = document.createElement("span");
let text4 = document.createTextNode(lotrCitiesArray.join(", "));
span4.appendChild(text4);
let placeHere4 = document.getElementById("num4");
placeHere4.appendChild(span4);

// num5: first 5 in lCA
let span5 = document.createElement("span");
let text5 = document.createTextNode(lotrCitiesArray.slice(0, 5).join(", "));
span5.appendChild(text5);
let placeHere5 = document.getElementById("num5");
placeHere5.appendChild(span5);

// num6: last 5 in lCA
let span6 = document.createElement("span");
let text6 = document.createTextNode(lotrCitiesArray.slice(3, lotrCitiesArray.length).join(", "));
                                                            // ^ doing 8 or not having anything work as well
span6.appendChild(text6);
let placeHere6 = document.getElementById("num6");
placeHere6.appendChild(span6);

// num7: 3-5 in lCA
let span7 = document.createElement("span");
let text7 = document.createTextNode(lotrCitiesArray.slice(2, 5).join(", "));
span7.appendChild(text7);
let placeHere7 = document.getElementById("num7");
placeHere7.appendChild(span7);

// num8: remove Rohan with splice
let span8 = document.createElement("span");
let removed8 = lotrCitiesArray.splice(2, 1);
let text8 = document.createTextNode(lotrCitiesArray.join(", "));
span8.appendChild(text8);
let placeHere8 = document.getElementById("num8");
placeHere8.appendChild(span8);

// num9: remove cities after DM with splice
let span9 = document.createElement("span");
let removed9 = lotrCitiesArray.splice(5, 2);
let text9 = document.createTextNode(lotrCitiesArray.join(", "));
span9.appendChild(text9);
let placeHere9 = document.getElementById("num9");
placeHere9.appendChild(span9);

// num10: add Rohan after gondor with splice
let span10 = document.createElement("span");
let added10 = lotrCitiesArray.splice(2, 0, "Rohan");
let text10 = document.createTextNode(lotrCitiesArray.join(", "));
span10.appendChild(text10);
let placeHere10 = document.getElementById("num10");
placeHere10.appendChild(span10);

// num11: make DM "-est -es" with splice
let span11 = document.createElement("span");
let removed11 = lotrCitiesArray.splice(5, 1, "Deadest Marshes");
let text11 = document.createTextNode(lotrCitiesArray.join(", "));
span11.appendChild(text11);
let placeHere11 = document.getElementById("num11");
placeHere11.appendChild(span11);

// num12: first 14 char of bT with slice
let span12 = document.createElement("span");
let text12 = document.createTextNode(bestThing.slice(0, 14));
span12.appendChild(text12);
let placeHere12 = document.getElementById("num12");
placeHere12.appendChild(span12);

// num13: last 12 char of bT with slice
let span13 = document.createElement("span");
let text13 = document.createTextNode(bestThing.slice(-12));
span13.appendChild(text13);
let placeHere13 = document.getElementById("num13");
placeHere13.appendChild(span13);

// num14: char between 23rd-38th position of bT with slice
let span14 = document.createElement("span");
let text14 = document.createTextNode(bestThing.slice(23, 38));
span14.appendChild(text14);
let placeHere14 = document.getElementById("num14");
placeHere14.appendChild(span14);

// num15: like 13 but with substring
let span15 = document.createElement("span");
let text15 = document.createTextNode(bestThing.substring(bestThing.length - 12));
span15.appendChild(text15);
let placeHere15 = document.getElementById("num15");
placeHere15.appendChild(span15);

// num16: like 14 but with substr
let span16 = document.createElement("span");
let text16 = document.createTextNode(bestThing.substr(23, 15));
span16.appendChild(text16);
let placeHere16 = document.getElementById("num16");
placeHere16.appendChild(span16);

// num17: index of "only" in bT
let span17 = document.createElement("span");
let text17 = document.createTextNode(bestThing.indexOf("only"));
span17.appendChild(text17);
let placeHere17 = document.getElementById("num17");
placeHere17.appendChild(span17);

// num18: index of last word in bT
let span18 = document.createElement("span");
let text18 = document.createTextNode(bestThing.lastIndexOf("bit"));
span18.appendChild(text18);
let placeHere18 = document.getElementById("num18");
placeHere18.appendChild(span18);

// num19: all cities with double vowels in gCC
let span19 = document.createElement("span");
// creatTN makes string
let text19 = gotCitiesCSV.split(",");
// if inside for loop, it clears on each iteration
arrayOfDoubleVowelCities = [];
for(let i = 0; i < text19.length; i++){
    cities = text19[i];
    if(cities.includes("aa") || cities.includes("ee") || cities.includes("ii") || cities.includes("oo") || cities.includes("uu")){
        arrayOfDoubleVowelCities.push(cities);
    }
}
span19.append(JSON.stringify(arrayOfDoubleVowelCities));
let placeHere19 = document.getElementById("num19");
placeHere19.appendChild(span19);

// num20: all cities that end with "or" in lCA
let span20 = document.createElement("span");
let text20 = lotrCitiesArray;
arrayOfEndWithOR = [];
for(let i = 0; i < text20.length; i++){
    cities = text20[i];
    if(cities.endsWith("or")){
        arrayOfEndWithOR.push(cities);
    }
}
span20.append(JSON.stringify(arrayOfEndWithOR));
let placeHere20 = document.getElementById("num20");
placeHere20.appendChild(span20);

// num21: all cities that start with "b" in bT
let span21 = document.createElement("span");
let text21 = bestThing.split(" ");
arrayOfStartsWithB = [];
for(let i = 0; i < text21.length; i++){
    words = text21[i];
    if(words.startsWith("b")){
        arrayOfStartsWithB.push(words);
    }
}
span21.append(JSON.stringify(arrayOfStartsWithB));
let placeHere21 = document.getElementById("num21");
placeHere21.appendChild(span21);

// num22: y/n if lCA has "Mirkwood"
function lcaIncludesMirkwood() {
    if(lotrCitiesArray.includes("Mirkwood")) {
        return "Yes"
    } else {
        return "No"
    }
}
let span22 = document.createElement("span");
let text22 = document.createTextNode(lcaIncludesMirkwood());
span22.appendChild(text22);
let placeHere22 = document.getElementById("num22");
placeHere22.appendChild(span22);

// num23: y/n if lCA has "Hollywood"
function lcaIncludesHollywood() {
    if(lotrCitiesArray.includes("Hollywood")) {
        return "Yes"
    } else {
        return "No"
    }
}
let span23 = document.createElement("span");
let text23 = document.createTextNode(lcaIncludesHollywood());
span23.appendChild(text23);
let placeHere23 = document.getElementById("num23");
placeHere23.appendChild(span23);

// num24: index of "Mirkwood" in lCA
let span24 = document.createElement("span");
let text24 = document.createTextNode(lotrCitiesArray.indexOf("Mirkwood"));
span24.appendChild(text24);
let placeHere24 = document.getElementById("num24");
placeHere24.appendChild(span24);

// num25: first city in lCA with 1+ word
let span25 = document.createElement("span");
let text25 = document.createTextNode(lotrCitiesArray.filter(city => city.includes(" ")));
span25.appendChild(text25);
let placeHere25 = document.getElementById("num25");
placeHere25.appendChild(span25);

// num26: reverse lCA
let span26 = document.createElement("span");
let text26 = document.createTextNode(lotrCitiesArray.reverse().join(", "));
span26.appendChild(text26);
let placeHere26 = document.getElementById("num26");
placeHere26.appendChild(span26);

// num27: alphabetize lCA
let span27 = document.createElement("span");
let text27 = document.createTextNode(lotrCitiesArray.sort().join(", "));
span27.appendChild(text27);
let placeHere27 = document.getElementById("num27");
placeHere27.appendChild(span27);

// num28: num of char lCA
let span28 = document.createElement("span");
lotrCitiesArray.sort(function(a, b){
    return a.length - b.length
});
let text28 = document.createTextNode(lotrCitiesArray.join(", "));
span28.appendChild(text28);
let placeHere28 = document.getElementById("num28");
placeHere28.appendChild(span28);

// num29: pop of lCA
let span29 = document.createElement("span");
lotrCitiesArray.pop();
let text29 = document.createTextNode(lotrCitiesArray.join(", "));
span29.appendChild(text29);
let placeHere29 = document.getElementById("num29");
placeHere29.appendChild(span29);

// num30: push back thing from 29 of lCA
let span30 = document.createElement("span");
lotrCitiesArray.push("Deadest Marshes");
let text30 = document.createTextNode(lotrCitiesArray.join(", "));
span30.appendChild(text30);
let placeHere30 = document.getElementById("num30");
placeHere30.appendChild(span30);

// num31: shift of lCA
let span31 = document.createElement("span");
lotrCitiesArray.shift();
let text31 = document.createTextNode(lotrCitiesArray.join(", "));
span31.appendChild(text31);
let placeHere31 = document.getElementById("num31");
placeHere31.appendChild(span31);

// num32: unshift thing from 31 of lCA
let span32 = document.createElement("span");
lotrCitiesArray.unshift("Rohan");
let text32 = document.createTextNode(lotrCitiesArray.join(", "));
span32.appendChild(text32);
let placeHere32 = document.getElementById("num32");
placeHere32.appendChild(span32);